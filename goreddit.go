// Package goreddit implements a basic client for the Reddit API
package goreddit

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

// Item describes a Reddit item
type Item struct {
	Title    string
	URL      string
	Comments int `json:"num_comments"`
}

type Response struct {
	Data struct {
		Children []struct {
			Data Item
		}
	}
}

// override fmt.Println
func (i Item) String() string {
	comment := ""
	switch i.Comments {
	case 0:
		// nothing
	case 1:
		comment = " (1 comment)"
	default:
		comment = fmt.Sprintf(" (%d comments)", i.Comments)
	}
	return fmt.Sprintf("Title: %s%s\nURL: %s", i.Title, comment, i.URL)
}

// Get fetches the most recent Items posted to the specified subreddit
func Get(url_base string) ([]Item, error) {
	url := fmt.Sprintf("http://reddit.com/r/%s.json", url_base)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(resp.Status)
	}
	r := new(Response)
	err = json.NewDecoder(resp.Body).Decode(r)
	if err != nil {
		return nil, err
	}
	items := make([]Item, len(r.Data.Children))
	for i, child := range r.Data.Children {
		items[i] = child.Data
	}

	return items, nil
}

/*

reddit json format

{ "kind": "Listing", "data": {"modhash": "", "children": [
    {"kind": "t3", "data": {
        "title": "IBM has just open-sourced 44,000 lines of blockchain code on GitHub",
        "url": "...",
        ...
    }},
    ..
]}}

godoc:
    godoc bitbucket.org/kenngsimply/goreddit
*/
