package main

import (
	"fmt"
	"log"

	"bitbucket.org/kenngsimply/goreddit"
)

func main() {
	items, err := goreddit.Get("golang")
	if err != nil {
		log.Fatal(err)
	}

	for _, item := range items {
		fmt.Println(item)
	}
}

/*
to run:
    go run geddit/main.go
*/
